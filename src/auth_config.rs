use crate::error::Error;
use serde::Deserialize;
use std::collections::HashMap;
use std::path::Path;
use std::str::FromStr;
#[derive(Deserialize, Clone, Debug)]
pub struct Auth {
    #[serde(default = "default_protocol")]
    pub protocol: String,
    pub domain: String,
    pub user: String,
    pub password: String,
}

fn default_protocol() -> String {
    "https://".into()
}

// %TAGNAME%
type Tag = String;
#[derive(Default, Debug, Clone, Deserialize)]
pub struct AuthConfig {
    #[serde(flatten)]
    inner: HashMap<Tag, Auth>,
}

impl AuthConfig {
    pub fn try_from_file<P: AsRef<Path>>(p: &Option<P>) -> Result<Self, Error> {
        if p.is_none() {
            return Ok(AuthConfig::default());
        }
        let p = p.as_ref().unwrap();
        if std::fs::metadata(p).is_err() {
            log::warn!(
                "'{}' does not exist! Fallback to empty auth config!",
                p.as_ref().display()
            );
            return Ok(AuthConfig::default());
        }
        let s = std::fs::read_to_string(p).map_err(|e| Error::Config(e.to_string()))?;
        AuthConfig::from_str(&s)
    }

    pub fn url_from_auth(&self, tag: &str) -> Option<String> {
        if let Some(auth) = self.inner.get(tag) {
            return Some(format!(
                "{}{}:{}@{}",
                auth.protocol, auth.user, auth.password, auth.domain
            ));
        }
        None
    }
}

impl FromStr for AuthConfig {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        toml::from_str(s).map_err(|e| Error::Config(format!("{}", e)))
    }
}
